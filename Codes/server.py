# import required libraries

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import numpy as np
from threading import Thread
import requests as rq
import re
from queue import Queue
import collections, itertools
from filelock import FileLock
from importlib import import_module
import os
from flask import Flask, render_template, Response
from _thread import *
import time
from base_camera import BaseCamera
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import io
import base64
from mpl_toolkits import mplot3d


# define a class for visualizing tracking plot on web page
class Camera(BaseCamera):
    """An emulated camera implementation that streams a repeated sequence of
    """

    @staticmethod
    def frames():
        while True:
            time.sleep(0.2)
            if len(Camera.imgs) > 0:
                yield Camera.imgs[-1]


app = Flask(__name__)


def mywebserver():  # defining a function for running flask
    @app.route('/')
    def index():
        """Video streaming home page."""
        return render_template('index.html')

    def gen(camera):
        """Video streaming generator function."""
        while True:
            frame = camera.get_frame()
            yield (b'--frame\r\n'
                   b'Content-Type: image/png\r\n\r\n' + frame + b'\r\n')
            # yield (b'<img src="data:image/png;base64,{}">'.format(frame))

    @app.route('/get')
    def get():
        return "x:" + str(q1.queue[-1]) + '<br> '+"y:" + str(q2.queue[-1]) + '<br> ' + "z:" + str(q3.queue[-1])

    @app.route('/video_feed')
    def video_feed():
        """Video streaming route. Put this in the src attribute of an img tag."""
        return Response(gen(Camera()),
                        mimetype='multipart/x-mixed-replace; boundary=frame')

    app.run(host='0.0.0.0', threaded=True)


start_new_thread(mywebserver, ())

# compile an engine for extracting position data from received string
p = re.compile('(-?\d+.\d+),(-?\d+.\d+),(-?\d+.\d+)')
fig = plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
ax = plt.axes(projection='3d')
l, = plt.plot([], [], 'r-', linewidth=1)
# defining queues for storing tracked positions
q1 = Queue()
q2 = Queue()
q3 = Queue()
ourimgs = []

def animate(i,ax,l, q1, q2, q3):
    # set figure size to a fixed size
    fig.set_size_inches(8, 6)
    animate.count += 1
    # send an http request to module and receive last position
    r = rq.get('http://192.168.1.2/get')
    b = r.content.decode('utf-8')
    x, y, z = p.search(b).groups()
    x, y, z = float(x), float(y), float(z)
    q1.put(x)
    q2.put(y)
    q3.put(z)
    ax.clear()
    ax.plot3D(list(q1.queue), list(q2.queue), list(q3.queue), 'r-')
    fig.canvas.draw()

    img = io.BytesIO()
    plt.savefig(img, format='png')
    img.seek(0)
    plot_url = base64.b64encode(img.getvalue()).decode()
    plot_url = img.read()
    ourimgs.append(plot_url)
    return l,

animate.count = 0
Camera.imgs = ourimgs

# plot tracked position
ani = animation.FuncAnimation(fig, animate, fargs=(ax, l, q1, q2, q3), interval=200, blit=True, repeat=False)
plt.show()