#include <Wire.h>           //I2C library
#include <ESP8266WiFi.h>                            
#include <ESP8266WebServer.h>
#include <Filters.h>

//output queue
float queue[100][3];
int queue_first = 0;
int queue_last = 0;
const int queue_size = 100;
//non stationary queue
float squeue[100][3];
int squeue_last = 0;
float last_data[3];         //last sent data
float stime = 0;            //non stationary period time                         

// MPU6050 Slave Device Address
const uint8_t MPU6050SlaveAddress = 0x68;

// Select SDA and SCL pins for I2C communication 
const uint8_t scl = 12;   //const uint8_t scl = D6;   #change
const uint8_t sda = 10;   //const uint8_t sda = D7;   #change

// sensitivity scale factor respective to full scale setting provided in datasheet 
const uint16_t AccelScaleFactor = 16384;
const uint16_t GyroScaleFactor = 131;

// MPU6050 few configuration register addresses
const uint8_t MPU6050_REGISTER_SMPLRT_DIV   =  0x19;
const uint8_t MPU6050_REGISTER_USER_CTRL    =  0x6A;
const uint8_t MPU6050_REGISTER_PWR_MGMT_1   =  0x6B;
const uint8_t MPU6050_REGISTER_PWR_MGMT_2   =  0x6C;
const uint8_t MPU6050_REGISTER_CONFIG       =  0x1A;
const uint8_t MPU6050_REGISTER_GYRO_CONFIG  =  0x1B;
const uint8_t MPU6050_REGISTER_ACCEL_CONFIG =  0x1C;
const uint8_t MPU6050_REGISTER_FIFO_EN      =  0x23;
const uint8_t MPU6050_REGISTER_INT_ENABLE   =  0x38;
const uint8_t MPU6050_REGISTER_ACCEL_XOUT_H =  0x3B;
const uint8_t MPU6050_REGISTER_SIGNAL_PATH_RESET  = 0x68;

int16_t AccelX, AccelY, AccelZ, Temperature, GyroX, GyroY, GyroZ;

void I2C_Write(uint8_t deviceAddress, uint8_t regAddress, uint8_t data){
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.write(data);
  Wire.endTransmission();
}

// read all 14 register
void Read_RawValue(uint8_t deviceAddress, uint8_t regAddress){
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.endTransmission();
  Wire.requestFrom(deviceAddress, (uint8_t)14);
  AccelX = (((int16_t)Wire.read()<<8) | Wire.read());
  AccelY = (((int16_t)Wire.read()<<8) | Wire.read());
  AccelZ = (((int16_t)Wire.read()<<8) | Wire.read());
  Temperature = (((int16_t)Wire.read()<<8) | Wire.read());
  GyroX = (((int16_t)Wire.read()<<8) | Wire.read());
  GyroY = (((int16_t)Wire.read()<<8) | Wire.read());
  GyroZ = (((int16_t)Wire.read()<<8) | Wire.read());
}

//configure MPU6050
void MPU6050_Init(){
  delay(150);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_SMPLRT_DIV, 0x07);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_PWR_MGMT_1, 0x01);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_PWR_MGMT_2, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_CONFIG, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_GYRO_CONFIG, 0x00);//set +/-250 degree/second full scale
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_ACCEL_CONFIG, 0x00);// set +/- 2g full scale
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_FIFO_EN, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_INT_ENABLE, 0x01);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_SIGNAL_PATH_RESET, 0x00);
  I2C_Write(MPU6050SlaveAddress, MPU6050_REGISTER_USER_CTRL, 0x00);
}

ESP8266WebServer server(80);

void handleRoot()       //only for checking our web server
{
  Serial.println("first page request");
  server.send(200,"text/plain","Tracker Http Server");
}

void handleStation()    //show connected station numbers
{
  String s = "stations:\t";
  s += WiFi.softAPgetStationNum();
  Serial.println(s);
  server.send(200,"text/plain",s);
}

void handleGet()        //send data to main server
{
  Serial.println("get data request");
  if (queue_last == queue_first)  //queue is empty. send last data.
  {
    String ss = String(last_data[0]) + "," + String(last_data[1]) + "," + String(last_data[2]);
    Serial.print(last_data[0] * 10);
    Serial.print(",");
    Serial.print(last_data[1] * 10);
    Serial.print(",");
    Serial.println(last_data[2] * 10);
    server.send(200,"text/plain",ss);
  }
  else  //pick a data from output queue
  {
    String ss = String(queue[queue_first][0]) + "," + String(queue[queue_first][1]) + "," + String(queue[queue_first][2]);
    server.send(200,"text/plain",ss);
    last_data[0] = queue[queue_first][0];
    last_data[1] = queue[queue_first][1];
    last_data[2] = queue[queue_first][2];
    Serial.print(last_data[0] * 10);
    Serial.print(",");
    Serial.print(last_data[1] * 10);
    Serial.print(",");
    Serial.println(last_data[2] * 10);
    queue_first++;
    if (queue_first == queue_size)
      queue_first = 0;
  }
}


float error = 0.15;
float velocity[3]{0,0,0};
float acceleration[3]{0,0,0};
float position[3]{0,0,0};
float timer[2];
float lowPassFrequency = 1;
float highPassFrequency = 1;

void setup() {
  Serial.begin(115200);
  Wire.begin(sda, scl);
  MPU6050_Init();
  WiFi.begin("your_ssid","your_pass");            //ssid and password of your modem
  WiFi.mode(WIFI_STA);
  while (WiFi.status() != WL_CONNECTED)       //wait until connect
  {
    delay(500);
    Serial.print(".");
  }
  server.on("/get", handleGet);
  server.on("\\get",handleGet);
  server.on("/", handleRoot);
  server.on("/station", handleStation);
  server.begin();
  Serial.println("server started!");
  Serial.println("server ip:");
  Serial.println(WiFi.localIP());             //server ip
  timer[0] = millis();
}

//filters
FilterOnePole filterLowPassX(LOWPASS , lowPassFrequency);
FilterOnePole filterHighPassX(HIGHPASS , highPassFrequency);
FilterOnePole filterLowPassY(LOWPASS , lowPassFrequency);
FilterOnePole filterHighPassY(HIGHPASS , highPassFrequency);
FilterOnePole filterLowPassZ(LOWPASS , lowPassFrequency);
FilterOnePole filterHighPassZ(HIGHPASS , highPassFrequency);

void loop() 
{
  timer[1] = millis() - timer[0];       //every period duration time
  timer[0] = millis();
  server.handleClient();
  double Ax, Ay, Az, T, Gx, Gy, Gz;
  Read_RawValue(MPU6050SlaveAddress, MPU6050_REGISTER_ACCEL_XOUT_H);    //read raw data
  //divide each with their sensitivity scale factor to reach the desired data
  Ax = (double)AccelX/AccelScaleFactor;
  Ay = (double)AccelY/AccelScaleFactor;
  Az = (double)AccelZ/AccelScaleFactor;
  Gx = (double)GyroX/GyroScaleFactor;
  Gy = (double)GyroY/GyroScaleFactor;
  Gz = (double)GyroZ/GyroScaleFactor;

  //filter input data
  filterLowPassX.input(Ax);
  filterHighPassX.input(filterLowPassX.output());
  Ax = filterHighPassX.output();
  filterLowPassY.input(Ay);
  filterHighPassY.input(filterLowPassY.output());
  Ay = filterHighPassY.output();
  filterLowPassZ.input(Az);
  filterHighPassZ.input(filterLowPassZ.output());
  Az = filterHighPassZ.output();

  //stationary recognization
  if (((Ax < error) && (Ax > (-1  * error))) && ((Ay < error) && (Ay > (-1  * error))) && ((Az < error) && (Az > (-1  * error))))
  {
    if (squeue_last != 0)     //squeue is not empty
    {
      velocity[0] /= squeue_last;   //velocity[0] contain last velocity data (before stationary) in axis X.
      velocity[1] /= squeue_last;
      velocity[2] /= squeue_last;
      stime /= squeue_last;         //time unit in non stationary period
      for(int i=0; i<squeue_last; i++)
      {
        if (queue_last != 0)
        {
          queue[queue_last][0] = queue[queue_last -1][0] + (squeue[i][0] - velocity[0]) * stime / 1000;
          queue[queue_last][1] = queue[queue_last -1][1] + (squeue[i][1] - velocity[1]) * stime / 1000;
          queue[queue_last][2] = queue[queue_last -1][2] + (squeue[i][2] - velocity[2]) * stime / 1000;
        }
        queue_last++;
        if (queue_last == queue_first)
          {
            queue_first++;
            if (queue_first == queue_size)
              queue_first = 0;
          }
        if (queue_last == queue_size) //go to the fisrt element until reaches to 
          queue_last = 0;
      }
      squeue_last = 0;
      stime = 0;
    }
    for (int i=0; i<3; i++)
    {
      velocity[i] = 0;
      acceleration[i] = 0;
    }
  }
  else
  {
    acceleration[0] = Ax;
    acceleration[1] = Ay;
    acceleration[2] = Az;
    velocity[0] += Ax * timer[1] / 1000 * 10;
    velocity[1] += Ay * timer[1] / 1000 * 10;
    velocity[2] += Az * timer[1] / 1000 * 10;
    squeue[squeue_last][0] = velocity[0];
    squeue[squeue_last][1] = velocity[1];
    squeue[squeue_last][2] = velocity[2];
    position[0] += velocity[0] * timer[1] / 1000;
    position[1] += velocity[1] * timer[1] / 1000;
    position[2] += velocity[2] * timer[1] / 1000;
    stime += timer[1];
    squeue_last++;
  }
  
//  String ss = String(Gx) + "," + String(Gy) + "," + String(Gz);
//  Serial.println(ss);
//  Serial.print(position[0]);
//  Serial.print(",");
//  Serial.print(position[1]);
//  Serial.print(",");
//  Serial.println(position[2]);
//  Serial.print(velocity[0]);
//  Serial.print(",");
//  Serial.print(velocity[1]);
//  Serial.print(",");
//  Serial.print(velocity[2]);
//  Serial.print(",");
//  Serial.print(acceleration[0]);
//  Serial.print(",");
//  Serial.print(acceleration[1]);
//  Serial.print(",");
//  Serial.println(acceleration[2]);
  delay(50);
}

